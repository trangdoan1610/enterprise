﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(CourseMonitoringReports.Startup))]
namespace CourseMonitoringReports
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
