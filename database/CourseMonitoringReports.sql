CREATE DATABASE CourseMonitoringReports
USE CourseMonitoringReports

CREATE TABLE [Admin]
(
	[Id] NVARCHAR(10) PRIMARY KEY,
	[Username] NVARCHAR(20),
	[Password] NVARCHAR(32),
	[Firstname] NVARCHAR(20),
	[Lastname] NVARCHAR(20),
	[Email]	NVARCHAR(50),
	[CreateBy] NVARCHAR(50),
	[DateCreate] DATE,
	[UpdateBy] NVARCHAR(50),
	[DateUpdate] DATE,
	[Locked] BIT
)

CREATE TABLE [Users]
(
	[Id] NVARCHAR(10) PRIMARY KEY,
	[Username] NVARCHAR(20),
	[Password] NVARCHAR(32),
	[Firstname] NVARCHAR(20),
	[Lastname] NVARCHAR(20),
	[Email]	NVARCHAR(50),
	[Gender] BIT,
	[DateOfBirth] DATE,
	[CreateBy] NVARCHAR(50),
	[DateCreate] DATE,
	[UpdateBy] NVARCHAR(50),
	[DateUpdate] DATE,
	[Locked] BIT
)

CREATE TABLE [TypeUser]
(
	[Id] NVARCHAR(10) PRIMARY KEY,
	[IdUser] NVARCHAR(10) REFERENCES [Users]([Id]),
	[Title] NVARCHAR(20),
	[Discription] NVARCHAR(50),
	[CreateBy] NVARCHAR(50),
	[DateCreate] DATE,
	[UpdateBy] NVARCHAR(50),
	[DateUpdate] DATE
)

CREATE TABLE [Faculty]
(
	[Id] NVARCHAR(10) PRIMARY KEY,
	[Name] NVARCHAR(20),
	[Discription] NVARCHAR(50),
	[CreateBy] NVARCHAR(50),
	[DateCreate] DATE,
	[UpdateBy] NVARCHAR(50),
	[DateUpdate] DATE,
	[Manager] NVARCHAR(50)
)

CREATE TABLE [Course]
(
	[Id] NVARCHAR(10) PRIMARY KEY,
	[Name] NVARCHAR(20),
	[CreateBy] NVARCHAR(50),
	[DateCreate] DATE,
	[UpdateBy] NVARCHAR(50),
	[DateUpdate] DATE
)

CREATE TABLE [ManageCF]
(
	[Id] NVARCHAR(10) PRIMARY KEY,
	[IdCourse] NVARCHAR(10) REFERENCES [Course]([Id]),
	[IdFaculty] NVARCHAR(10) REFERENCES [Faculty]([Id]),
	[DateCreate] DATE
)

CREATE TABLE [ManageCU]
(
	[Id] NVARCHAR(10) PRIMARY KEY,
	[IdCourse] NVARCHAR(10) REFERENCES [Course]([Id]),
	[IdUser] NVARCHAR(10) REFERENCES [Users]([Id]),
	[DateCreate] DATE
)

CREATE TABLE [ManageFU]
(
	[Id] NVARCHAR(10) PRIMARY KEY,
	[IdUser] NVARCHAR(10) REFERENCES [Users]([Id]),
	[IdFaculty] NVARCHAR(10) REFERENCES [Faculty]([Id]),
	[DateCreate] DATE,
	[CreateBy] NVARCHAR(50),
	[Positive] NVARCHAR(50)
)

CREATE TABLE [Report]
(
	[Id] NVARCHAR(10) PRIMARY KEY,
	[IdCourse] NVARCHAR(10) REFERENCES [Course]([Id]),
	[Title] NVARCHAR(20),
	[Discription] NVARCHAR(50),
	[DateCreate] DATE,
	[CreateBy] NVARCHAR(50)
)

CREATE TABLE [ReportRating]
(
	[Id] NVARCHAR(10) PRIMARY KEY,
	[IdReport] NVARCHAR(10) REFERENCES [Report]([Id]),
	[Title] NVARCHAR(20),
	[Comment] NVARCHAR(100),
	[CreateBy] NVARCHAR(50),
	[DateCreate] DATE,
	[UpdateBy] NVARCHAR(50),
	[DateUpdate] DATE,
	[Discription] NVARCHAR(50),
	[From] NVARCHAR(50),
	[To] NVARCHAR(50),
	[Status] NVARCHAR(50)
)


USE CourseMonitoringReports

DROP TABLE [Admin]
DROP TABLE [TypeUser]
DROP TABLE [ManageFU]
DROP TABLE [ManageCF]
DROP TABLE [ManageCU]
DROP TABLE [ReportRating]
DROP TABLE [Report]
DROP TABLE [Course]
DROP TABLE [Faculty]
DROP TABLE [Users]
